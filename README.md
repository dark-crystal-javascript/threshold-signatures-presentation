
### Group threshold signatures

- A useful consensus mechanism for groups?
- Groups < 10 members?
- maybe something more 'subjective' for big groups

---

#### Two applications of group threshold signatures

### 1. 'key reissuance'

- you assign a 'support group' who are empowered to make assertions on your behalf should you loose your secret key
- "the support group of alice" says: "this is the new public key of alice"
---

#### Two applications of group threshold signatures

### 2. 'group encryption'
- a hard problem for p2p (no source of truth):
- to compute a group key (using DH), we need consensus about who the members are
- group signatures could show group aggreement about a member joining or leaving.

---

### Signing

- sign(message, secret key) => signature
- verify(message, signature, public key) => true or false

---

### BLS Signature Scheme

- a,b,c all individually sign the same message
- a.pk + b.pk + c.pk = aggregate public key
- a.sig + b.sig + c.sig = aggregate signature
- validate(aggregate pk, aggregate sig) = true!
- can do other operations which work on a sk, pk, sig tuple
- including shamirs secret sharing(!)

---
We are going to walk through Distributed Key Generation.
In DKG a group of members generate a "shared secret key" that none of them
know individal and public key. When a threshold amount of group members agree to sign on
the same message then then anyone can combine the signatures into a single
signature that can be verified against the groups public key
---
1. each member will generate a secret key used to sign messages for the group.
2. a group verification vector which contains the groups public key as well as the information need to derive any of the members public key.

---

1. Each member will "setup" and generate a verification vector and secret
key contrubution share for every other member
2. Each member post their verifcation vector publicly
3. Each member sends their key contrubution share each other member
4. When a member recieves a contrubution share it validates it against
the senders verifcation vector and saves it
5. After members receive all thier contribution shares they compute
their secret key for the group

---

to setup a group first we need a set a threshold. The threshold is the
number of group participants need to create a valid siganture for the group

```js
const threshold = 4
```

each member in the group needs a unique ID. What the id is doesn't matter
but it does need to be imported into bls-lib as a secret key
```js
const members = [10314, 30911, 25411, 8608, 31524, 15441, 23399].map(id => {
    const sk = new bls.SecretKey()
    sk.setHashOf(Buffer.from([id]))
    return {
      id: sk,
      recievedShares: []
    }
  })
```
---

this stores an array of verifcation vectors. One for each Member

```js
const vvecs = []
```

each member need to first create a verification vector and a secret key
contribution for every other member in the group (including itself!)

```js
members.forEach(id => {
    const {verificationVector, secretKeyContribution} = dkg.generateContribution(bls, members.map(m => m.id), threshold)
```
the verification vector should be posted publically so that everyone
in the group can see it

```js
vvecs.push(verificationVector)
```
---

Each secret sk contribution is then encrypted and sent to the member it is for.
when a group member receives its share, it verifies it against the
verification vector of the sender and then saves it
```js
secretKeyContribution.forEach((sk, i) => {
  const member = members[i]
  const verified = dkg.verifyContributionShare(bls, member.id, sk, verificationVector)
  if (!verified) {
    throw new Error('invalid share!')
  }
  member.recievedShares.push(sk)
})
```
---

now each members adds together all received secret key contributions shares to get a
single secretkey share for the group used for signing message for the group
```js
members.forEach((member, i) => {
  const sk = dkg.addContributionShares(member.recievedShares)
  member.secretKeyShare = sk
})
```
---

Now any one can add together the all verification vectors posted by the
members of the group to get a single verification vector of for the group
```js
const groupsVvec = dkg.addVerificationVectors(vvecs)
```
the groups verifcation vector contains the groups public key. The group's
public key is the first element in the array
```js
const groupsPublicKey = groupsVvec[0]
```
---

now we can select any 4 members to sign on a message
```js
const message = 'hello world'
const sigs = []
const signersIds = []
for (let i = 0; i < threshold; i++) {
  const sig = members[i].secretKeyShare.sign(message)
  sigs.push(sig)
  signersIds.push(members[i].id)
}
```

---

then anyone can combine the signatures to get the groups signature
the resulting signature will also be the same no matter which members signed
```js
const groupsSig = new bls.Signature()
groupsSig.recover(sigs, signersIds)

var verified = groupsPublicKey.verify(groupsSig, message)
```
---

we can also use the groups verification vector to derive any of the members
public key
```js
const member = members[4]
const pk1 = new bls.PublicKey()
pk1.share(groupsVvec, member.id)

const pk2 = member.secretKeyShare.getPublicKey()
```
